﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX.DirectInput;

namespace WpfApp4
{
    class JoystickPL
    {
        DirectInput directInput;
        SlimDX.DirectInput.Joystick joystick;
        System.Timers.Timer timer;

        public delegate void EventAxis(SlimDX.DirectInput.JoystickState state);
        public event EventAxis OnReadJoystickAxis;
        public delegate void EventJs(bool[] buttons);
        public event EventJs OnReadJoystickButton;

        public JoystickPL()
        {

        }
        public void InitializationJoystic()
        {
            directInput = new DirectInput();
            var joystickGuid = Guid.Empty;

            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AllDevices))
                joystickGuid = deviceInstance.InstanceGuid;

            if (joystickGuid == Guid.Empty)
                foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
                    joystickGuid = deviceInstance.InstanceGuid;

            joystick = new Joystick(directInput, joystickGuid);

            var allEffects = joystick.GetEffects();
            foreach (var effectInfo in allEffects)
                Console.WriteLine("Effect available {0}", effectInfo.Name);

            joystick.Properties.BufferSize = 128;

            joystick.Acquire();
        }
        public void InitializationTimer()
        {
            timer = new System.Timers.Timer();
            timer.Interval = 40;
            timer.Elapsed += Timer_Elapsed;
        }
        public void TimerStart()
        {
            timer.Start();
        }
        public void TimerStop()
        {
            timer.Stop();
        }
        enum Type
        {
            moveTopX,
            moveTopY,
            moveBottomX,
            moveBottomY,
            moveBottomZ
        }
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            joystick.Poll();
            SlimDX.DirectInput.JoystickState state = joystick.GetCurrentState();
            bool[] buttons = state.GetButtons();
            OnReadJoystickAxis(state);
            OnReadJoystickButton(buttons);
        }
    }
}
