﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.JScript;
using SlimDX.DirectInput;
using SlimDX.Windows;

namespace WpfApp4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string _text = null;
        public MainWindow()
        {
            InitializeComponent();

            JoystickPL joystickPL = new JoystickPL();
            joystickPL.OnReadJoystickAxis += JoystickPL_OnReadJoystickAxis;
            joystickPL.OnReadJoystickButton += JoystickPL_OnReadJoystickButton;
            joystickPL.InitializationJoystic();
            joystickPL.InitializationTimer();
            joystickPL.TimerStart();

        }

        private void JoystickPL_OnReadJoystickButton(bool[] buttons)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                _block.Text = (buttons[0]) ? "Fire" : "0";
                //_block.Text = (buttons[1]) ? "1" : "0";
                //_block.Text = (buttons[2]) ? "2" : "0";
                //_block.Text = (buttons[3]) ? "3" : "0";
               //  _block.Text = (buttons[4]) ? "4" : "0";
            
            }));
        }

        private void JoystickPL_OnReadJoystickAxis(JoystickState state)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                _blockX.Text = state.X.ToString();
                _blockY.Text = state.Y.ToString();
                _blockZ.Text = state.Z.ToString();

                _blockTopX.Text = state.RotationX.ToString();
                _blockTopY.Text = state.RotationY.ToString();
            }));
        }

        void MainForJoystick()
        {
            // Initialize DirectInput
            DirectInput directInput = new DirectInput();

            // Find a Joystick Guid
            var joystickGuid = Guid.Empty;

            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AllDevices))
                joystickGuid = deviceInstance.InstanceGuid;

            // If Gamepad not found, look for a Joystick
            if (joystickGuid == Guid.Empty)
                foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
                    joystickGuid = deviceInstance.InstanceGuid;

            // If Joystick not found, throws an error

            // Instantiate the joystick
            SlimDX.DirectInput.Joystick joystick = new Joystick(directInput, joystickGuid);


            // Query all suported ForceFeedback effects
            var allEffects = joystick.GetEffects();
            foreach (var effectInfo in allEffects)
                Console.WriteLine("Effect available {0}", effectInfo.Name);

            // Set BufferSize in order to use buffered data.
            joystick.Properties.BufferSize = 128;

            // Acquire the joystick
            joystick.Acquire();

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 40;
            timer.Elapsed += (s, e) =>
            {
                joystick.Poll();

                SlimDX.DirectInput.JoystickState state = joystick.GetCurrentState();

                bool[] buttons = state.GetButtons();


                System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    if (buttons[0])
                    {
                        _block.Text = "Fire";
                    }
                    if (buttons[1])
                    {
                        _block.Text = "1";
                    }
                    if (buttons[2])
                    {
                        _block.Text = "2";
                    }
                    if (buttons[3])
                    {
                        _block.Text = "3";
                    }
                    if (buttons[4])
                    {
                        _block.Text = "4";
                    }

                    _blockX.Text = state.X.ToString();
                    _blockY.Text = state.Y.ToString();
                    _blockZ.Text = state.Z.ToString();

                    _blockTopX.Text = state.RotationX.ToString();
                    _blockTopY.Text = state.RotationY.ToString();
                }));

            };
            //start the timer
            timer.Start();
        }
     
    }
}
